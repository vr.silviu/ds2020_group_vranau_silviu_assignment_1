//
//  RepoHelper.swift
//  DoctorApp
//
//  Created by mac on 29/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

class RepoHelper {
    
    static public func shouldDecode<T: Codable>(key: UserType, ofType: T.Type) -> T? {
        let defaults = UserDefaults.standard
        let data: Data?
        switch key {
        case .caregiver:
            data = defaults.value(for: .caregiverKey)
        case .doctor:
            data = defaults.value(for: .doctorKey)
        case .patient:
            data = defaults.value(for: .patientKey)
        }
        do {
              let decoder = JSONDecoder()
              let res = try decoder.decode(APIResponse<T>.self, from: data ?? Data())
              return res.result
          } catch _ {
              print("Error data was not a \(T.self)")
          }
          return nil
      }
}


