//
//  AlertHelper.swift
//  DoctorApp
//
//  Created by mac on 29/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation
import UIKit

struct TextFieldProperties {
    
}

extension UITextField {
    func setProperties(placeHolder: String, keyboardType: UIKeyboardType? = .default, keyboardApperance: UIKeyboardAppearance? = .default, autocorrectionType: UITextAutocorrectionType? = .default) {
        self.placeholder = placeHolder

    }
}

struct AlertHelper {
    
    static func present(alertController: UIAlertController, actions: AlertHelper.Action..., with textFieldConfigurations: [((UITextField) -> ())], from controller: UIViewController) {
        for action in actions {
            alertController.addAction(action.alertAction)
        }
        
        for config in textFieldConfigurations {
            alertController.addTextField(configurationHandler: config)
        }
        
        controller.present(alertController, animated: true, completion: nil)
    }
}

extension AlertHelper {
    enum Action {
        case save(handler: (() -> Void)?)
        case retry(handler: (() -> Void)?)
        case update(handler: (() -> Void)?)
        case close
        
        // Returns the title of our action button
        private var title: String {
            switch self {
            case .save:
                return "OK"
            case .retry:
                return "Retry"
            case .close:
                return "Close"
            case .update:
                return "Update"
            }
            
        }
        
        // Returns the completion handler of our button
        private var handler: (() -> Void)? {
            switch self {
            case .save(let handler):
                return handler
            case .retry(let handler):
                return handler
            case .update(let handler):
                return handler
            case .close:
                return nil
            }
        }
        
        var alertAction: UIAlertAction {
            return UIAlertAction(title: title, style: .default, handler: { _ in
                if let handler = self.handler {
                    handler()
                }
            })
        }
    }
}
