//
//  AppCoordinator.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

import UIKit

protocol Coordinator {
    var navigationController: UINavigationController { get set }
    func start()
}

class MainCoordinator: Coordinator {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = LoginViewController(nibName: String(describing: LoginViewController.self), bundle: nil)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func navigateToDoctorPage() {
        let vc = DoctorViewController(nibName: String(describing: DoctorViewController.self), bundle: nil)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func navigateToPatientPage() {
        let vc = PatientViewController(nibName: String(describing: PatientViewController.self), bundle: nil)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func navigateToCaregiverPage() {
        let vc = CaregiverViewController(nibName: String(describing: CaregiverViewController.self), bundle: nil)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func navigateToPatientEdit() {
        let vc = PatientEditViewController(nibName: String(describing: PatientEditViewController.self), bundle: nil)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func navigateToCaregiverEdit() {
        let vc = CaregiverEditViewController(nibName: String(describing: CaregiverEditViewController.self), bundle: nil)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func navigateToMedicationEdit() {
        let vc = MedicationEditViewController(nibName: String(describing: MedicationEditViewController.self), bundle: nil)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
}
