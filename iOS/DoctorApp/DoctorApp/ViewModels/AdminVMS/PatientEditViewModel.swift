//
//  PatientEditViewModel.swift
//  DoctorApp
//
//  Created by mac on 29/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol PatientEditDelegate: class {
    func reloadData(newValue: [Patient])
    func setDataSource(patients: [Patient])
}

class PatientEditViewModel {
    
    private let patientService: PatientServiceProtocol
    private var patients: [Patient] = []
    weak var delegate: PatientEditDelegate?
    
    init(patientService: PatientServiceProtocol = PatientService()) {
        self.patientService = patientService
    }
    
    func fetchPatients() {
        patientService.fetchPatients() { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let err): print(err)
            case .success(let patientsResult):
                self.patients = patientsResult
                self.delegate?.reloadData(newValue: self.patients)
            }
        }
    }
    
    func deletePatient(with index: Int) {
        patientService.deletePatient(id: patients[index].id ?? -1) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let err): print(err)
            case .success(_):
                self.patients.remove(at: index)
                //self.delegate?.reloadData(newValue: self.patients)
            }
        }
    }
    
    func addMedicalPlan(idPatient: String, plan: MedicalPlanPost) {
        patientService.createMedicalPlan(idPatient: idPatient, plan: plan) {  [weak self] result in
            guard let _ = self else { return }
            switch result {
            case .failure(let err): print(err)
            case .success(let res):
                print(res.message)
            }
        }
    }
    
    func postPatient(patient: Patient) {
        patientService.postPatient(patient: patient) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let err): print(err)
            case .success(let patientResult):
                if(self.shouldUpdate(patient: patientResult.result ?? Patient()) != -1) {
                    let ind = self.shouldUpdate(patient: patientResult.result ?? Patient())
                    self.patients[ind] = patientResult.result ?? Patient()
                    self.delegate?.reloadData(newValue: self.patients)
                } else {
                    self.patients.append(patientResult.result ?? Patient())
                    self.delegate?.reloadData(newValue: self.patients)
                }
            }
        }
    }
    
    private func shouldUpdate(patient: Patient) -> Int {
        for index in 0 ..< self.patients.count {
            if(patient.id == self.patients[index].id ) {
                self.patients[index] = patient
                return index
            }
        }
        return -1
    }
    
    func getPatientByIndex(index: Int) -> Patient {
        let patient = patients[index]
        return Patient(id: patient.id ?? -1, name: patient.name ?? "", email: patient.email, address: patient.address, medicalRecord: patient.medicalRecord, password: patient.password )
    }
}
