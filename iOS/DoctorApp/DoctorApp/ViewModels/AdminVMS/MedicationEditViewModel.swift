//
//  MedicationEditViewModel.swift
//  DoctorApp
//
//  Created by mac on 30/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol MedicationEditDelegate: class {
    func reloadData()
    func deleteRows(indexPath: IndexPath)
    //func setDataSource(medications: [Medication])
}

class MedicationEditViewModel {
    
    private let medicationService: MedicationServiceProtocol
    private(set) var medications: [Medication] = []
    weak var delegate: MedicationEditDelegate?
    var deleteCompletion: (() -> ())?
    
    
    init(medicationService: MedicationServiceProtocol = MedicationService()) {
        self.medicationService = medicationService
    }
    
    func loadMeds() {
        medicationService.fetchMedication() { [weak self] res in
            guard let self = self else { return }
            switch res {
            case .failure(let err): print(err)
            case .success(let meds):
                self.medications = meds
                self.delegate?.reloadData()
            }
        }
    }
    
    func deleteMed(indexPath: IndexPath) {
        medicationService.deleteMedication(id: medications[indexPath.row].id ?? -1) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let err): print(err)
            case .success(_):
                self.medications.remove(at: indexPath.row)
                self.delegate?.deleteRows(indexPath: indexPath)
            }
        }
    }
    
    func postMed(med: Medication) {
        medicationService.postPatient(medication: med ) { [weak self] res in
            guard let self = self else { return }
            switch res {
            case .failure(let err): print(err)
            case .success(let med):
                if(self.shouldUpdate(med: med.result ?? Medication()) != -1) {
                    let ind = self.shouldUpdate(med: med.result ?? Medication())
                    self.medications[ind] = med.result ?? Medication()
                    self.delegate?.reloadData()
                } else {
                    self.medications.append(med.result ?? Medication())
                    self.delegate?.reloadData()
                }
            }
        }
    }
    
    private func shouldUpdate(med: Medication) -> Int {
        for index in 0 ..< self.medications.count {
            if(med.id == self.medications[index].id ) {
                self.medications[index] = med
                return index
            }
        }
        return -1
    }
    
    
    func configureCell(_ itemView: MedicationTableCellProtocol, at index: Int) {
        itemView.configure(med: medications[index])
    }
    
}
