//
//  CaregiverEditViewModel.swift
//  DoctorApp
//
//  Created by mac on 30/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol CaregiverEditDelegate: class {
    func reloadData()
    func deleteRows(indexPath: IndexPath)
}

class CaregiverEditViewModel {
    
    private let caregiverService: CaregiverServiceProtocol
    private(set) var caregivers: [Caregiver] = []
    weak var delegate: CaregiverEditDelegate?
    init(caregiverService: CaregiverServiceProtocol = CaregiverService()) {
        self.caregiverService = caregiverService
    }
    
    func loadCaregivers() {
        caregiverService.fetchCaregivers() { [weak self] res in
            guard let self = self else { return }
            switch res {
            case .failure(let err): print(err)
            case .success(let c):
                self.caregivers = c
                self.delegate?.reloadData()
            }
        }
    }
    
    func deleteCaregiver(indexPath: IndexPath) {
        caregiverService.deleteCaregiver(id: caregivers[indexPath.row].id ?? -1) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let err): print(err)
            case .success(_):
                self.caregivers.remove(at: indexPath.row)
                self.delegate?.deleteRows(indexPath: indexPath)
            }
        }
    }
    
    func postCaregiver(caregiver: Caregiver) {
        caregiverService.postCaregiver(caregiver: caregiver ) { [weak self] res in
            guard let self = self else { return }
            switch res {
            case .failure(let err): print(err)
            case .success(let care):
                if(self.shouldUpdate(caregiver: care.result ?? Caregiver()) != -1) {
                    let ind = self.shouldUpdate(caregiver: care.result ?? Caregiver())
                    self.caregivers[ind] = care.result ?? Caregiver()
                    self.delegate?.reloadData()
                } else {
                    self.caregivers.append(care.result ?? Caregiver())
                    self.delegate?.reloadData()
                }
            }
        }
    }
    
    func addPatientToCaregiver(indexCaregiver: Int, idPatient: String) {
        let idCaregiver = caregivers[indexCaregiver].id ?? -1
        
        caregiverService.addPatientToCaregiver(idCaregiver: String(idCaregiver), idPatient: idPatient) {  [weak self] res in
            guard let _ = self else { return }
            switch res {
            case .failure(let err): print(err)
            case .success(let res):
                print(res)
            }
        }
    }
    
    private func shouldUpdate(caregiver: Caregiver) -> Int {
        for index in 0 ..< self.caregivers.count {
            if(caregiver.id == self.caregivers[index].id ) {
                self.caregivers[index] = caregiver
                return index
            }
        }
        return -1
    }
    
    func configureCell(_ itemView: CaregiverCellProtocol, at index: Int) {
        itemView.configure(care: caregivers[index])
    }
}
