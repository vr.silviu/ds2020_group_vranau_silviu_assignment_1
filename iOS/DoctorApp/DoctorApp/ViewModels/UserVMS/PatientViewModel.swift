//
//  PatientViewModel.swift
//  DoctorApp
//
//  Created by mac on 01/11/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol PatientDelegate: class {
    func configureData(text: String)
}

class PatientViewModel {
    
    private let patientService: PatientServiceProtocol
    private var patient: Patient = Patient()
    weak var delegate: PatientDelegate?
    
    init(patientService: PatientServiceProtocol = PatientService()) {
        self.patientService = patientService
    }
    
    func fetchPatient() {
        let idCurrentUser = getCurrentUserId()
        patientService.fetchPatient(by: idCurrentUser) { res in
            switch res {
            case .failure(let err): print(err)
            case .success(let res):
                guard let patient = res.result else { return }
                self.patient = patient
                print(patient.getPatientDetails())
                self.delegate?.configureData(text: patient.getPatientDetails())
            }
        }
    }
    
    func getCurrentUserId() -> Int {
        return RepoHelper.shouldDecode(key: .patient, ofType: Caregiver.self)?.id ?? -1
    }
    
}

