//
//  CaregiverViewModel.swift
//  DoctorApp
//
//  Created by mac on 31/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol CaregiverDelegate: class {
    func configureData()
}

class CaregiverViewModel {
    
    private let caregiverService: CaregiverServiceProtocol
    private(set) var caregiver: Caregiver = Caregiver()
    weak var delegate: CaregiverDelegate?
    
    init(caregiverService: CaregiverServiceProtocol = CaregiverService()) {
        self.caregiverService = caregiverService
    }
    
    func loadCaregiver() {
        let idCurrentUser = getCurrentUserId()
        caregiverService.fetchCaregiver(by: idCurrentUser) { res in
            switch res {
            case .failure(let err): print(err)
            case .success(let res):
                guard let care = res.result else { return }
                self.caregiver = care
                self.delegate?.configureData()
            }
        }
    }
    
    func getCurrentUserId() -> Int {
        return RepoHelper.shouldDecode(key: .caregiver, ofType: Caregiver.self)?.id ?? -1
    }
}
