//
//  LoginViewModel.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol LoginViewModelProtocol {
    
}
protocol LoginDelegate: class {
    func didSignIn(userType: UserType)
}



class LoginViewModel {
    
    private let loginService: LoginServiceProtocol
    weak var delegate: LoginDelegate?
    
    init(loginService: LoginServiceProtocol = LoginService()) {
        self.loginService = loginService
    }
    func loadData() {
        
    }
    
    func login(email: String, password: String) {
        loginService.authenticate(email: email, password: password) { [weak self] result in
            guard let self = self else { return }
            
            switch(result) {
            case.success(let data):
                switch(data) {
                case .caregiver:
                    print(RepoHelper.shouldDecode(key: .caregiver, ofType: Caregiver.self)?.id ?? -2)
                    self.delegate?.didSignIn(userType: .caregiver)
                case .doctor:
                    print(RepoHelper.shouldDecode(key: .doctor, ofType: Doctor.self)?.email ?? "")
                    self.delegate?.didSignIn(userType: .doctor)
                case .patient:
                    print(RepoHelper.shouldDecode(key: .patient, ofType: Patient.self)?.email ?? "")
                    self.delegate?.didSignIn(userType: .patient)
                }
            case.failure(let error): print(error)
            }
        }
    }
}
