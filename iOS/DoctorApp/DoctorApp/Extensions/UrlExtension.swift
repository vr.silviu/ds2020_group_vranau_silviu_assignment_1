//
//  UrlExtension.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}

enum ParsError: Error {
    case requestError
}

extension String {
    static let loginEndPoint = "/login"
    static let patientsEndPoint = "/patients"
    static let medicatonEndPoint = "/medications"
    static let caregiversEndPoint = "/caregivers"
    static let addPatientToCaregiverEndPoint = "/caregivers/addPatientToCaregiver"
    static let medicalPLan = "/medicalPlan/add"
}

extension URL {
    static let base = URL(string: "https://enigmatic-crag-44442.herokuapp.com/api/v1")
}

extension URLSession {
    func request<T: Codable, D: Codable>(url: URL, method: HTTPMethod, parameters: [String: Any], data: D?, completion: @escaping (Result<T, Error>) -> ()) {
        var request = URLRequest(url: url)
        
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json"
        ]
        if(method != .get) {
            do {
                request.httpBody = try JSONEncoder().encode(data)
            } catch let requestError {
                completion(.failure(requestError))
                return
            }
        }
        request.httpMethod = method.rawValue
        
        self.dataTask(with: request) { (data: Data?, response: URLResponse?, responseError: Error?) in
            guard let httpResponse = response as? HTTPURLResponse else {
                return
            }
            
            if httpResponse.statusCode == 200 {
                do {
                    let data1 = try JSONDecoder().decode(T.self, from: data!)
                    DispatchQueue.main.async {
                        completion(.success(data1))
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(.failure(error))
                    }
                    
                }
                return
            }
            
            DispatchQueue.main.async {
                completion(.failure(ParsError.requestError))
            }
        }.resume()
    }
    
}

