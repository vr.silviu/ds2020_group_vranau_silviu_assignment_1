//
//  UserDefaultsExtension.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    public struct Key<Value> {
        fileprivate let name: String
        public init(_ name: String) {
            self.name = name
        }
    }
}

extension UserDefaults {
    
    public func value<Value>(for key: Key<Value>) -> Value? {
        return object(forKey: key.name) as? Value
    }
    
    public func set<Value>(_ value: Value, for key: Key<Value>) {
        set(value, forKey: key.name)
    }
    
    public func removeValue<Value>(for key: Key<Value>) {
        removeObject(forKey: key.name)
    }
}

extension UserDefaults.Key where Value == Data {
    static let launchCount = Self("launchCount")
    static let doctorKey = Self("doctorKey")
    static let patientKey = Self("patientKey")
    static let caregiverKey = Self("caregiverKey")
}



