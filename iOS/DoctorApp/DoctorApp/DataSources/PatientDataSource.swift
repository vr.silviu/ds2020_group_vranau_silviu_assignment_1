//
//  PatientDataSource.swift
//  DoctorApp
//
//  Created by mac on 29/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation
import UIKit

class PatientDataSource: NSObject, UITableViewDataSource {
    var patients: [Patient] = []
    var deleteHandler: ((Int) -> ())?
    init(patients: [Patient]) {
        self.patients = patients
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PatientConstants.patientCellId, for: indexPath) as? PatientTableViewCell else { return UITableViewCell() }
        cell.configure(patient: patients[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            patients.remove(at: indexPath.row)
            deleteHandler?(indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
   
    
   
    
}
