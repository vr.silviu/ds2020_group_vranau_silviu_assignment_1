//
//  LoginService.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol  LoginServiceProtocol {
    func authenticate(email: String, password: String, completion: @escaping (Result<UserType, Error>) -> Void)
}

enum UserType {
    case doctor
    case caregiver
    case patient
    
    var stringVal: String {
        switch self {
        case .doctor: return "doctor"
        case .caregiver: return "caregiver"
        case .patient: return "patient"
        }
    }
}

class LoginService: LoginServiceProtocol {
    private let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func authenticate(email: String, password: String, completion: @escaping (Result<UserType, Error>) -> Void) {
        let url = URL.base?.appendingPathComponent(String.loginEndPoint)
        guard let urlLogin = url else { return }
        var request = URLRequest(url: urlLogin)
        request.httpMethod = "POST"
        let parameters = ["email": email, "password": password]
        guard let httpBody = try? JSONEncoder().encode(parameters) else { return }
        
        request.httpBody = httpBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        session.dataTask(with: request) { [weak self] data, response, error in
            guard let self = self else { return }
            if let _ = response {
            }
            
            if let data = data {
                do {
                    let result = try JSONDecoder().decode(Response.self, from: data)
                    let userType = self.saveCurrentUser(data: data, res: result)
                    
                    DispatchQueue.main.async {
                        completion(.success(userType))
                    }
                    
                } catch {
                    DispatchQueue.main.async {
                        completion(.failure(error))
                    }
                }
            }
        }.resume()
    }
    

    
    private func saveCurrentUser(data: Data, res: Response) -> UserType {
        let defaults = UserDefaults.standard
        if(res.message.contains("Doctor")) {
            defaults.set(data, for: .doctorKey)
            return .doctor
        } else if(res.message.contains("Caregiver")) {
            defaults.set(data, for: .caregiverKey)
            return .caregiver
        } else {
            defaults.set(data, for: .patientKey)
            return .patient
        }
    }
}
