//
//  PatientService.swift
//  DoctorApp
//
//  Created by mac on 29/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol PatientServiceProtocol {
    func fetchPatients(completion: @escaping (Result<[Patient], Error>) -> ())
    func postPatient(patient: Patient, completion: @escaping (Result<APIResponse<Patient>, Error>) -> ())
    func deletePatient(id: Int, completion: @escaping (Result<APIResponse<Patient>, Error>) -> ())
    func fetchPatient(by id: Int, completion: @escaping (Result<APIResponse<Patient>, Error>) -> ())
    func createMedicalPlan(idPatient: String, plan: MedicalPlanPost, completion: @escaping (Result<APIResponse<MedicalPlan>, Error>) -> ())
}

class PatientService: PatientServiceProtocol {
    
    private let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func fetchPatients(completion: @escaping (Result<[Patient], Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent(String.patientsEndPoint) else { return }
        session.request(url: url, method: .get, parameters: [:], data: Patient()) { (result: Result<[Patient], Error>) in
            completion(result)
        }
    }
    
    func postPatient(patient: Patient, completion: @escaping (Result<APIResponse<Patient>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent(String.patientsEndPoint) else { return }
        session.request(url: url, method: .post, parameters: [:], data: patient) { (result: Result<APIResponse<Patient>, Error>) in
            completion(result)
        }
    }
    
    func deletePatient(id: Int, completion: @escaping (Result<APIResponse<Patient>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent("\(String.patientsEndPoint)/\(id)") else { return }
        session.request(url: url, method: .delete, parameters: [:], data: Patient()) { (result: Result<APIResponse<Patient>, Error>) in
            completion(result)
        }
    }
    
    func fetchPatient(by id: Int, completion: @escaping (Result<APIResponse<Patient>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent("\(String.patientsEndPoint)/\(id)") else { return }
        session.request(url: url, method: .get, parameters: [:], data: Patient()) { (result: Result<APIResponse<Patient>, Error>) in
            completion(result)
        }
    }
    
    func createMedicalPlan(idPatient: String, plan: MedicalPlanPost, completion: @escaping (Result<APIResponse<MedicalPlan>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent("\(String.medicalPLan)/\(idPatient)") else { return }
        session.request(url: url, method: .post, parameters: [:], data: plan) { (result: Result<APIResponse<MedicalPlan>, Error>) in
            completion(result)
        }
    }
}
