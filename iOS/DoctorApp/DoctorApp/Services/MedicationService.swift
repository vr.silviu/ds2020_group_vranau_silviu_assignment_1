//
//  MedicationService.swift
//  DoctorApp
//
//  Created by mac on 30/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol  MedicationServiceProtocol {
    func fetchMedication(completion: @escaping (Result<[Medication], Error>) -> ())
    func postPatient(medication: Medication, completion: @escaping (Result<APIResponse<Medication>, Error>) -> ())
    func deleteMedication(id: Int, completion: @escaping (Result<APIResponse<Medication>, Error>) -> ())
}

class MedicationService: MedicationServiceProtocol {
    
    
    private let session: URLSession
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func fetchMedication(completion: @escaping (Result<[Medication], Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent(String.medicatonEndPoint) else { return }
        session.request(url: url, method: .get, parameters: [:], data: Medication()) { (result: Result<[Medication], Error>) in
            completion(result)
        }
    }
    
    func postPatient(medication: Medication, completion: @escaping (Result<APIResponse<Medication>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent(String.medicatonEndPoint) else { return }
        session.request(url: url, method: .post, parameters: [:], data: medication) { (result: Result<APIResponse<Medication>, Error>) in
            completion(result)
        }
    }
    
    func deleteMedication(id: Int, completion: @escaping (Result<APIResponse<Medication>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent("\(String.medicatonEndPoint)/\(id)") else { return }
        session.request(url: url, method: .delete, parameters: [:], data: Patient()) { (result: Result<APIResponse<Medication>, Error>) in
            completion(result)
        }
    }
}
