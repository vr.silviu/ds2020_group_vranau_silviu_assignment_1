//
//  CaregiverService.swift
//  DoctorApp
//
//  Created by mac on 30/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

protocol CaregiverServiceProtocol {
    func fetchCaregivers(completion: @escaping (Result<[Caregiver], Error>) -> ())
    func postCaregiver(caregiver: Caregiver, completion: @escaping (Result<APIResponse<Caregiver>, Error>) -> ())
    func deleteCaregiver(id: Int, completion: @escaping (Result<APIResponse<Caregiver>, Error>) -> ())
    func fetchCaregiver(by id: Int, completion: @escaping (Result<APIResponse<Caregiver>, Error>) -> ())
    func addPatientToCaregiver(idCaregiver: String, idPatient: String, completion: @escaping (Result<APIResponse<Caregiver>, Error>) -> ())
}

class CaregiverService: CaregiverServiceProtocol {
    
    private let session: URLSession
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func fetchCaregivers(completion: @escaping (Result<[Caregiver], Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent(String.caregiversEndPoint) else { return }
        session.request(url: url, method: .get, parameters: [:], data: Caregiver()) { (result: Result<[Caregiver], Error>) in
            completion(result)
        }
    }
    
    func postCaregiver(caregiver: Caregiver, completion: @escaping (Result<APIResponse<Caregiver>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent(String.caregiversEndPoint) else { return }
        session.request(url: url, method: .post, parameters: [:], data: caregiver) { (result: Result<APIResponse<Caregiver>, Error>) in
            completion(result)
        }
    }
    
    func deleteCaregiver(id: Int, completion: @escaping (Result<APIResponse<Caregiver>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent("\(String.caregiversEndPoint)/\(id)") else { return }
        session.request(url: url, method: .delete, parameters: [:], data: Caregiver()) { (result: Result<APIResponse<Caregiver>, Error>) in
            completion(result)
        }
    }
    
    func fetchCaregiver(by id: Int, completion: @escaping (Result<APIResponse<Caregiver>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent("\(String.caregiversEndPoint)/\(id)") else { return }
        session.request(url: url, method: .get, parameters: [:], data: Caregiver()) { (result: Result<APIResponse<Caregiver>, Error>) in
            completion(result)
        }
    }
    
    func addPatientToCaregiver(idCaregiver: String, idPatient: String, completion: @escaping (Result<APIResponse<Caregiver>, Error>) -> ()) {
        guard let url = URL.base?.appendingPathComponent("\(String.addPatientToCaregiverEndPoint)/\(idCaregiver)/\(idPatient)") else { return }
        
        session.request(url: url, method: .post, parameters: [:], data: Caregiver()) { (result: Result<APIResponse<Caregiver>, Error>) in
            completion(result)
        }
        
    }
    
    
}
