//
//  Doctor.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import Foundation

struct Response: Codable {
    let status: Int
    let message: String
    
}
struct APIResponse<T: Codable>: Codable {
    let status: Int
    let message: String
    var result: T?
}

struct Doctor: Codable {
    let id: Int
    let name: String
    let email: String
    let doctor: Bool
}

struct MedicalPlan: Codable {
    var id: Int
    var doctorName: String
    var treatmentPeriod: String
    var medications: [Medication]?
}

struct MedicalPlanPost: Codable {

    var doctorName: String
    var treatmentPeriod: String
    var medicationIds: [Int]
}

struct Patient: Codable {
    let id: Int?
    let name: String?
    let email: String?
    let birthDate: String?
    let gender: String?
    let address: String?
    let medicalRecord: String?
    let password: String?
    let patient: Bool?
    var medicalPlans: [MedicalPlan]?
    
    init(id: Int? = nil, name: String? = nil, email: String? = nil, birthDate: String? = nil, gender: String? = nil, address: String? = nil, medicalRecord: String? = nil, patient: Bool? = nil, password: String? = nil) {
        self.id = id
        self.name = name
        self.email = email
        self.birthDate = birthDate
        self.gender = gender
        self.address = address
        self.medicalRecord = medicalRecord
        self.patient = patient
        self.password = password
    }
    
    public var description: String {
        return "\(id ?? -1) \n \(name ?? "") \n \(email ?? "") \n \(address ?? "") \n \(medicalRecord ?? "")"
    }
    
    func getPatientDetails() -> String {
        guard let medicalPlans = medicalPlans else { return ""}
        var f = ""
        //let r = String(format: "Medical plan: %@ \nDoctor name: %@ \nTreatment period: %@\n")
        for m in medicalPlans {
            var medicationString = ""
            var r = ""
            for med in m.medications ?? [] {
                let temp = String(format: "\t\t\tMedication name: %@\n\t\t\tSide effects: %@\n\t\t\tDosage: %@ \n\n", med.name ?? "", med.sideEffects ?? "", med.dosage ?? "")
                medicationString.append(temp)
            }
            r = String(format: "Medical plan: %@ \nDoctor name: %@ \nTreatment period: %@\n\n\t\tMedications:\n%@", String(m.id), m.doctorName, m.treatmentPeriod, medicationString)
            f.append(r)
        }
        return f
    }
}

struct Caregiver: Codable {
    var id: Int?
    var name: String?
    var email: String?
    var birthDate: String?
    var gender: String?
    var address: String?
    var caregiver: Bool?
    var patients: [Patient]?
    var password: String?
    
    public func getPatientsName() -> String {
        guard let patients = patients else {return ""}
        let ps = patients.map { $0.name ?? ""}
        var r = ""
        let _ = ps.map { r.append("\($0)\n\n")}
        return r
    }
    public var description: String {
        return " \(name ?? "") \n \(email ?? "") \n \(address ?? "") \n \(password ?? "")"
    }
}

extension Caregiver {
    init(id: Int? = nil, name: String? = nil, email: String? = nil, birthDate: String? = nil, gender: String? = nil, address: String? = nil, caregiver: Bool? = nil, password: String? = nil) {
        self.id = id
        self.name = name == "" ? nil : name
        self.email = email == "" ? nil : email
        self.birthDate = birthDate
        self.gender = gender
        self.address = address == "" ? nil : address
        self.caregiver = caregiver
        self.password = password == "" ? nil : password
    }
}

struct Medication: Codable {
    var id: Int?
    var name: String?
    var sideEffects: String?
    var dosage: String?
    
    public var description: String {
        return " \(id ?? -1) \n \(name ?? "") \n \(sideEffects ?? "") \n \(dosage ?? "")"
    }
    
    init(id: Int? = nil, name: String? = nil, sideEffects: String? = nil, dosage: String? = nil) {
        self.id = id
        self.name = name
        self.sideEffects = sideEffects
        self.dosage = dosage
    }
}

