//
//  LoginViewController.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

struct LoginConstants {
    static let title = "Login"
}

class LoginViewController: UIViewController {
    
    weak var coordinator: MainCoordinator?
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    lazy var loginViewModel: LoginViewModel = {
        return LoginViewModel()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        loginViewModel.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        coordinator?.navigationController.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        coordinator?.navigationController.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        loginViewModel.login(email: emailTextField.text ?? "", password: passwordTextField.text ?? "")
    }
    
    func configureView() {
        title = LoginConstants.title
    }
}

extension LoginViewController: LoginDelegate {
    func didSignIn(userType: UserType) {
        switch userType {
        case .doctor:
            coordinator?.navigateToDoctorPage()
        case .patient:
            coordinator?.navigateToPatientPage()
        case .caregiver:
            coordinator?.navigateToCaregiverPage()
        }
    }
}
