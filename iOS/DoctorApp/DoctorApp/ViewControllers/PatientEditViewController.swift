//
//  PatientEditViewController.swift
//  DoctorApp
//
//  Created by mac on 29/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

struct PatientConstants {
    static let patientCellId = "patientCell"
}

class PatientEditViewController: UIViewController {
    
    lazy var patientEditViewModel: PatientEditViewModel = {
        return PatientEditViewModel()
    }()
    weak var coordinator: MainCoordinator?
    @IBOutlet weak var patientTableView: UITableView!
    var patientDataSource: PatientDataSource = PatientDataSource(patients: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Patients"
        configureViewModel()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let addButton = UIBarButtonItem(barButtonSystemItem: .add ,target: self, action: #selector(addButtonTapped))
        let addPlanButton = UIBarButtonItem(barButtonSystemItem: .compose ,target: self, action: #selector(medicalPlanButtonTapped))
        self.navigationItem.rightBarButtonItems  = [addPlanButton, addButton]
    }
    
    @objc func addButtonTapped(){
        showAlert(title: "Create Patient", message: "")
    }
    
    @objc func medicalPlanButtonTapped(){
        let alertController = UIAlertController(title: "Add medical plan", message: "" , preferredStyle: .alert)
        let create = UIAlertAction(title: "Create", style: .default) { (action) in
            guard let tfs = alertController.textFields else { return }
            let patientId = tfs[0].text ?? ""
            let medicationIds = tfs[3].text ?? ""
            let medicationIdsIntArray = medicationIds.components(separatedBy: " ").compactMap{ Int($0) }
            
            let medPlan = MedicalPlanPost(doctorName: tfs[1].text ?? "", treatmentPeriod: tfs[2].text ?? "", medicationIds: medicationIdsIntArray)
            self.patientEditViewModel.addMedicalPlan(idPatient: patientId, plan: medPlan)
            print(medPlan)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        let ph = ["Patient id", "Doctor name", "Treatment period", "Medication ids"]
        for i in 0..<4 {
            alertController.addTextField() { tf in
                tf.placeholder = ph[i]
            }
        }
        alertController.addAction(create)
        alertController.addAction(cancel)
        present(alertController, animated: true)
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        let action = AlertHelper.Action.save() {
            guard let tfs = alertController.textFields else { return }
            let patient = Patient(name: tfs[0].text ?? "", email: tfs[1].text ?? "", birthDate: tfs[6].text ?? "", gender: tfs[2].text ?? "", address: tfs[3].text ?? "", medicalRecord: tfs[4].text ?? "", password: tfs[5].text ?? "")
            self.patientEditViewModel.postPatient(patient: patient)
            print(patient)
        }
        AlertHelper.present(alertController: alertController, actions: action, .close, with: configureAlertTextFields(), from: self)
    }
    
    private func configureAlertTextFields() -> [((UITextField) -> ())] {
        let nameConfig = getConfig(placeHolder: "Name")
        let emailConfig = getConfig(placeHolder: "Email")
        let genderConfig = getConfig(placeHolder: "Gender")
        let addressConfig = getConfig(placeHolder: "Address")
        let medicalRecordConfig = getConfig(placeHolder: "Medical Record")
        let passwordConfig = getConfig(placeHolder: "Password")
        let birthDateConfig = getConfig(placeHolder: "Birthdate")
        let configArray = [nameConfig, emailConfig, genderConfig, addressConfig, medicalRecordConfig, passwordConfig, birthDateConfig]
        return configArray
    }
    
    private func getConfig(placeHolder: String) -> ((UITextField) -> ()) {
        let config: ((UITextField) -> ()) = { tf in
            tf.setProperties(placeHolder: placeHolder)
        }
        return config
    }
    
    private func configureViewModel() {
        patientEditViewModel.delegate = self
        patientEditViewModel.fetchPatients()
    }
    
    private func configureTableView() {
        patientTableView.delegate = self
        patientTableView.dataSource = patientDataSource
        patientDataSource.deleteHandler = { index in
            self.patientEditViewModel.deletePatient(with: index)
        }
        let nib = UINib(nibName: String(describing: PatientTableViewCell.self), bundle: nil)
        self.patientTableView.register(nib, forCellReuseIdentifier: PatientConstants.patientCellId)
    }
}

extension PatientEditViewController: PatientEditDelegate {
    func setDataSource(patients: [Patient]) {
        patientDataSource.patients = patients
    }
    
    func reloadData(newValue: [Patient]) {
        patientDataSource.patients = newValue
        patientTableView.reloadData()
    }
}

extension PatientEditViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        showAlertForUpdate(title: "Update patient", index: indexPath.row)
    }
    
    
    private func showAlertForUpdate(title: String, index: Int) {
        
        let alertController = UIAlertController(title: title, message: "\(patientEditViewModel.getPatientByIndex(index: index).description)" , preferredStyle: .alert)
        let action = AlertHelper.Action.update() {
            guard let tfs = alertController.textFields else { return }
            let id = self.patientEditViewModel.getPatientByIndex(index: index).id ?? -1
            let patient = Patient(id: id ,name: tfs[0].text ?? nil == "" ? nil : tfs[0].text ?? nil, email: tfs[1].text ?? nil == "" ? nil : tfs[1].text ?? nil, birthDate: tfs[6].text ?? nil == "" ? nil : tfs[6].text ?? nil, gender: tfs[2].text ?? nil == "" ? nil : tfs[2].text ?? nil, address: tfs[3].text ?? nil == "" ? nil : tfs[3].text ?? nil, medicalRecord: tfs[4].text ?? nil == "" ? nil : tfs[4].text ?? nil, password: tfs[5].text ?? nil == "" ? nil : tfs[5].text ?? nil)
            
            self.patientEditViewModel.postPatient(patient: patient)
        }
        AlertHelper.present(alertController: alertController, actions: action, .close, with: configureAlertTextFields(), from: self)
    }
}
