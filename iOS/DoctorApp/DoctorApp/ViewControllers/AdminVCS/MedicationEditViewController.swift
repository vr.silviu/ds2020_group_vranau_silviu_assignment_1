//
//  MedicationEditViewController.swift
//  DoctorApp
//
//  Created by mac on 30/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

class MedicationEditViewController: UIViewController {
    
    weak var coordinator: MainCoordinator?
    lazy var medicationEditViewModel: MedicationEditViewModel = {
        return MedicationEditViewModel()
    }()
    @IBOutlet weak var medicationTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Medications"
        medicationEditViewModel.loadMeds()
        medicationEditViewModel.delegate = self /// NUUU UITA DE ASTA BA omule
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let addButton = UIBarButtonItem(barButtonSystemItem: .add ,target: self, action: #selector(addButtonTapped))
        self.navigationItem.rightBarButtonItems  = [addButton]
    }
    
    @objc func addButtonTapped(){
        showAlert(title: "Create med", message: nil)
    }
    
    private func configureTableView() {
        medicationTableView.delegate = self
        medicationTableView.dataSource = self
        let nib = UINib(nibName: String(describing: MedicationTableViewCell.self), bundle: nil)
        self.medicationTableView.register(nib, forCellReuseIdentifier: "medicationId")
    }
    
    private func showAlert(title: String, message: String?) {
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save", style: .default) { (action) in
            guard let tfs = alertController.textFields else { return }
            let med = Medication(name: tfs[0].text, sideEffects: tfs[1].text, dosage: tfs[2].text)
            self.medicationEditViewModel.postMed(med: med)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        let placeHolders = ["Name", "Side effects", "Dosage"]
        for i in 0..<3 {
            alertController.addTextField() { tf in
                tf.placeholder = placeHolders[i]
            }
        }
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
    
}

extension MedicationEditViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medicationEditViewModel.medications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "medicationId", for: indexPath) as? MedicationTableViewCell else { return UITableViewCell() }
        
        medicationEditViewModel.configureCell(cell, at: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            medicationEditViewModel.deleteMed(indexPath: indexPath)
        }
    }
}

extension MedicationEditViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        showAlertForUpdate(title: "Update med" , message: medicationEditViewModel.medications[indexPath.row].description, index: indexPath.row)
    }
    
    private func showAlertForUpdate(title: String, message: String?, index: Int) {
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        let updateAction = UIAlertAction(title: "Update", style: .default) { (action) in
            guard let tfs = alertController.textFields else { return }
            let med = Medication(id: self.medicationEditViewModel.medications[index].id, name: tfs[0].text == "" ? nil : tfs[0].text , sideEffects: tfs[1].text == "" ? nil : tfs[1].text, dosage: tfs[2].text == "" ? nil : tfs[2].text)
            
            self.medicationEditViewModel.postMed(med: med)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        let placeHolders = ["Name", "Side effects", "Dosage"]
        for i in 0..<3 {
            alertController.addTextField() { tf in
                tf.placeholder = placeHolders[i]
            }
        }
        alertController.addAction(updateAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
}

extension MedicationEditViewController: MedicationEditDelegate {
    func deleteRows(indexPath: IndexPath) {
        self.medicationTableView.deleteRows(at: [indexPath], with: .top)
    }
    
    func reloadData() {
        medicationTableView.reloadData()
    }
}
