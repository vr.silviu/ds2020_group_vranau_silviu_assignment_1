//
//  CaregiverEditViewController.swift
//  DoctorApp
//
//  Created by mac on 30/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

class CaregiverEditViewController: UIViewController {
    
    weak var coordinator: MainCoordinator?
    @IBOutlet weak var caregiverTableView: UITableView!
    lazy var caregiverEditViewModel: CaregiverEditViewModel = {
        return CaregiverEditViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Caregivers"
        caregiverEditViewModel.loadCaregivers()
        caregiverEditViewModel.delegate = self
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let addButton = UIBarButtonItem(barButtonSystemItem: .add ,target: self, action: #selector(addButtonTapped))
        self.navigationItem.rightBarButtonItems  = [addButton]
    }
    
    @objc func addButtonTapped(){
        showAlert(title: "Create caregiver", message: nil)
    }
    
    private func showAlert(title: String, message: String?) {
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save", style: .default) { (action) in
            guard let tfs = alertController.textFields else { return }
            let care = Caregiver(name: tfs[0].text, email: tfs[1].text, birthDate: tfs[2].text, gender: tfs[3].text, address: tfs[4].text, password: tfs[5].text)
            self.caregiverEditViewModel.postCaregiver(caregiver: care)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        let placeHolders = ["Name", "Email", "BirthDate", "Gender", "Address", "Password"]
        for i in 0..<6 {
            alertController.addTextField() { tf in
                tf.placeholder = placeHolders[i]
            }
        }
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
    private func configureTableView() {
        caregiverTableView.delegate = self
        caregiverTableView.dataSource = self
        let nib = UINib(nibName: String(describing: CaregiverTableViewCell.self), bundle: nil)
        self.caregiverTableView.register(nib, forCellReuseIdentifier: "caregiverId")
    }
}

extension CaregiverEditViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return caregiverEditViewModel.caregivers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "caregiverId", for: indexPath) as? CaregiverTableViewCell else { return UITableViewCell() }
        
        caregiverEditViewModel.configureCell(cell, at: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        showAlertForUpdate(title: "Update caregiver" , message: caregiverEditViewModel.caregivers[indexPath.row].description, index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            caregiverEditViewModel.deleteCaregiver(indexPath: indexPath)
        }
    }
    
    private func showAlertForUpdate(title: String, message: String?, index: Int) {
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        let addPatientAction = UIAlertAction(title: "Add Patient", style: .default) {(action) in
             guard let tfs = alertController.textFields else { return }
            let idPatient = tfs[4].text ?? ""
            self.caregiverEditViewModel.addPatientToCaregiver(indexCaregiver: index, idPatient: idPatient)
        }
        let updateAction = UIAlertAction(title: "Update", style: .default) { (action) in
            guard let tfs = alertController.textFields else { return }
            let care = Caregiver(id: self.caregiverEditViewModel.caregivers[index].id, name: tfs[0].text, email: tfs[1].text, address: tfs[2].text, password: tfs[3].text)
            self.caregiverEditViewModel.postCaregiver(caregiver: care)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        let placeHolders = ["Name", "Email", "Address", "Password"]
        for i in 0..<4 {
            alertController.addTextField() { tf in
                tf.placeholder = placeHolders[i]
            }
        }
        
        alertController.addTextField() { tf in
            tf.placeholder = "Patient id"
        }
        alertController.addAction(updateAction)
        alertController.addAction(cancelAction)
        alertController.addAction(addPatientAction)
        present(alertController, animated: true)
    }
}

extension CaregiverEditViewController: CaregiverEditDelegate {
    func deleteRows(indexPath: IndexPath) {
        caregiverTableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    func reloadData() {
        caregiverTableView.reloadData()
    }
}
