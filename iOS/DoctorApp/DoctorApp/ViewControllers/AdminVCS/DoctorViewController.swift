//
//  DoctorViewController.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

class DoctorViewController: UIViewController {
    
    weak var coordinator: MainCoordinator?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Doctor"
    }
    @IBAction func patientButtonTapped(_ sender: Any) {
        coordinator?.navigateToPatientEdit()
    }
    @IBAction func caregiverButtonTapped(_ sender: Any) {
        coordinator?.navigateToCaregiverEdit()
    }
    @IBAction func medicationButtonTapped(_ sender: Any) {
        coordinator?.navigateToMedicationEdit()
    }
}
