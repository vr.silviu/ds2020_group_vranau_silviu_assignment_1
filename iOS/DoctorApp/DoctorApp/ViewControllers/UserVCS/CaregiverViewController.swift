//
//  CaregiverViewController.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

class CaregiverViewController: UIViewController {
    
    weak var coordinator: MainCoordinator?
    @IBOutlet weak var patientsTextView: UITextView!
    lazy var caregiverViewModel: CaregiverViewModel = {
        return CaregiverViewModel()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Caregiver"
        caregiverViewModel.loadCaregiver()
        caregiverViewModel.delegate = self
        
    }
}

extension CaregiverViewController: CaregiverDelegate {
    func configureData() {
        self.patientsTextView.text = caregiverViewModel.caregiver.getPatientsName()
    }
}
