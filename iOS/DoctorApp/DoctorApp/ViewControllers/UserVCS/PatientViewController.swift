//
//  PatientViewController.swift
//  DoctorApp
//
//  Created by mac on 28/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

class PatientViewController: UIViewController {
    
    weak var coordinator: MainCoordinator?
    @IBOutlet weak var plansTextView: UITextView!
    lazy var patientViewModel: PatientViewModel = {
        return PatientViewModel()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Patient"
        patientViewModel.fetchPatient()
        patientViewModel.delegate = self
    }
}

extension PatientViewController: PatientDelegate {
    func configureData(text: String) {
        self.plansTextView.text = text
    }
}
