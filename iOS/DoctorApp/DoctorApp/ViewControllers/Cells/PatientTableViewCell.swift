//
//  PatientTableViewCell.swift
//  DoctorApp
//
//  Created by mac on 29/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

class PatientTableViewCell: UITableViewCell {

    @IBOutlet weak var patientNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(patient: Patient) {
        self.patientNameLabel.text = patient.name
    }

}
