//
//  CaregiverTableViewCell.swift
//  DoctorApp
//
//  Created by mac on 30/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

protocol CaregiverCellProtocol {
    func configure(care: Caregiver)
}


class CaregiverTableViewCell: UITableViewCell {

    @IBOutlet weak var caregiverLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension CaregiverTableViewCell: CaregiverCellProtocol {
    func configure(care: Caregiver) {
        self.caregiverLabel.text = care.name
    }
}
