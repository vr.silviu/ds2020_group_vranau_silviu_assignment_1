//
//  MedicationTableViewCell.swift
//  DoctorApp
//
//  Created by mac on 30/10/2020.
//  Copyright © 2020 silviuApps. All rights reserved.
//

import UIKit

protocol MedicationTableCellProtocol {
    func configure(med: Medication)
}

class MedicationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var medNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension MedicationTableViewCell: MedicationTableCellProtocol {
    func configure(med: Medication) {
        medNameLabel.text = med.name
    }
}
