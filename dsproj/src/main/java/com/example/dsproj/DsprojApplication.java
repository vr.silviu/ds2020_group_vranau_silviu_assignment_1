package com.example.dsproj;

import com.example.dsproj.dao.*;
//import com.example.dsproj.dao.UserDAO;
import com.example.dsproj.model.*;
//import com.example.dsproj.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class DsprojApplication implements CommandLineRunner {

	DoctorDAO doctorDAO;
	PatientDAO patientDAO;
	MedicationDAO medicationDAO;
	CaregiverDAO caregiverDAO;
	MedicalPlanDAO medicalPlanDAO;
	MedicationAndMedicalPlanDAO medicationAndMedicalPlanDAO;
	@Autowired
	public DsprojApplication(DoctorDAO doctorDAO, PatientDAO patientDAO,
							 MedicationDAO medicationDAO,
							 CaregiverDAO caregiverDAO, MedicalPlanDAO medicalPlanDAO,
							 MedicationAndMedicalPlanDAO medicationAndMedicalPlanDAO) {
		this.doctorDAO = doctorDAO;
		this.patientDAO = patientDAO;
		this.medicationDAO = medicationDAO;
		this.caregiverDAO = caregiverDAO;
		this.medicalPlanDAO = medicalPlanDAO;
		this.medicationAndMedicalPlanDAO = medicationAndMedicalPlanDAO;
	}

	public static void main(String[] args) {
		SpringApplication.run(DsprojApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Doctor doctor = new Doctor("Doctor John", "john@gmail.com", "123", true);
		doctorDAO.save(doctor);

		Caregiver caregiver = new Caregiver("caregiver1", "caregiver@", "12/02/2000", "male", "12Ionesc", "123");
		Caregiver caregiver1 = new Caregiver("caregiver2", "caregiver2@", "1/02/2000", "male", "1Mai", "123");
		caregiverDAO.save(caregiver);
		caregiverDAO.save(caregiver1);


		Patient patient = new Patient("Foo", "foo@gmail.com", "123",
				"12/03/1900",
				"Male", "21 December",
				"Is kinda healty", true);
		Patient patient1 = new Patient("July", "july@gmail.com", "123",
				"03/01/1931",
				"Female", "Mihai viteazu",
				"Sick poor her", true);
		Patient patient2 = new Patient("Ana", "ana@gmail.com", "123",
				"03/01/1221",
				"Female", "gorgescu",
				"Sick poor her", true);

		patient1.setCaregiver(caregiver);
		patient.setCaregiver(caregiver);
		patientDAO.save(patient);
		patientDAO.save(patient1);
		patientDAO.save(patient2);

		Set<Medication> medications = new HashSet<>();
		Medication medication1 = new Medication("aspirin", "stomach ulcer", "1/day");
		Medication medication2 = new Medication("paracetamol", "some, not many", "2/day");
		Medication medication3 = new Medication("parasinus", "it is bleah", "3/day");
		Medication medication4 = new Medication("faringosept", "preatty sweet", "3/day");
		Medication medication5 = new Medication("vitamin C", "no side effect", "1/day");
		medicationDAO.save(medication1);
		medicationDAO.save(medication2);
		medicationDAO.save(medication3);
		medicationDAO.save(medication4);
		medicationDAO.save(medication5);

		medications.add(medication1);
		medications.add(medication2);
		medications.add(medication3);




		MedicalPlan medicalPlan1 = new MedicalPlan("Doctor John", "3 months treatment");
		MedicalPlan medicalPlan2 = new MedicalPlan("Doctor John", "1 months treatment");
		MedicalPlan medicalPlan3 = new MedicalPlan("Doctor John", "12 months treatment");
		//MedicalPlan medicalPlan4 = new MedicalPlan("Doctor John", "4 months treatment");
		medicalPlan1.setPatient(patient);
		medicalPlan2.setPatient(patient);
		medicalPlan3.setPatient(patient1);
		//medicalPlan4.setPatient(patient2);
		medicalPlanDAO.save(medicalPlan1);
		medicalPlanDAO.save(medicalPlan2);
		medicalPlanDAO.save(medicalPlan3);
		//medicalPlanDAO.save(medicalPlan4);

		MedicationAndMedicalPlan medicationAndMedicalPlan1 =
				new MedicationAndMedicalPlan(medication1.getId(), medicalPlan1.getId());
		MedicationAndMedicalPlan medicationAndMedicalPlan2 =
				new MedicationAndMedicalPlan(medication1.getId(), medicalPlan2.getId());
		MedicationAndMedicalPlan medicationAndMedicalPlan3 =
				new MedicationAndMedicalPlan(medication2.getId(), medicalPlan1.getId());
		MedicationAndMedicalPlan medicationAndMedicalPlan4 =
				new MedicationAndMedicalPlan(medication5.getId(), medicalPlan3.getId());

		medicationAndMedicalPlanDAO.save(medicationAndMedicalPlan1);
		medicationAndMedicalPlanDAO.save(medicationAndMedicalPlan2);
		medicationAndMedicalPlanDAO.save(medicationAndMedicalPlan3);
		medicationAndMedicalPlanDAO.save(medicationAndMedicalPlan4);









	}
}
