package com.example.dsproj.api;


import com.example.dsproj.dto.MedicationDTO;
import com.example.dsproj.dto.PatientDTO;
import com.example.dsproj.model.ApiResponse;
import com.example.dsproj.service.MedicationService;
import com.example.dsproj.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("api/v1/medications")
@RestController
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired

    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping
    public List<MedicationDTO> getAllMedication() {
        return medicationService.getAllMedication();
    }

    @PostMapping
    public ApiResponse addOrUpdateMedication(@Valid @NonNull @RequestBody MedicationDTO medicationDTO) {
        return medicationService.addOrUpdateMedication(medicationDTO);
    }

    @DeleteMapping(path = "{id}")
    public ApiResponse deleteMedicationById(@PathVariable("id") Long id) {
        return medicationService.deleteMedication(id);
    }

    @GetMapping(path = "{id}")
    public ApiResponse getMedicationById(@PathVariable("id") Long id) {
        return medicationService.getMedicationById(id);
    }
}
