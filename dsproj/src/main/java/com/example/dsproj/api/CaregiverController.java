package com.example.dsproj.api;


import com.example.dsproj.dto.CaregiverDTO;
import com.example.dsproj.dto.MedicationDTO;
import com.example.dsproj.dto.PatientDTO;
import com.example.dsproj.model.ApiResponse;
import com.example.dsproj.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RequestMapping("api/v1/caregivers")
@RestController
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired

    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping
    public List<CaregiverDTO> getAllCaregivers() {
        return caregiverService.getAllCaregivers();
    }

    @PostMapping
    public ApiResponse addOrUpdateCaregiver(@Valid @NonNull @RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.addOrUpdateCaregiver(caregiverDTO);
    }


    @RequestMapping("/addPatientToCaregiver/{idCaregiver}/{idPatient}")
    @PostMapping(path="{id}")
    public ApiResponse addPatientToCaregiver(@PathVariable("idCaregiver") Long idCaregiver,
                                             @PathVariable("idPatient") Long idPatient){
        return caregiverService.addPatientsToCaregiver(idCaregiver, idPatient);
    }

    @RequestMapping("/removePatientFromCaregiver/{idCaregiver}/{idPatient}")
    @PostMapping
    public ApiResponse removePatientFromCaregiver(@PathVariable("idCaregiver") Long id,
                                                  @PathVariable("idPatient") Long idPatient){
        return caregiverService.removePatientsFromCaregiver(id, idPatient);
    }

    @DeleteMapping(path = "{id}")
    public ApiResponse deleteCaregiverById(@PathVariable("id") Long id) {
        return caregiverService.deleteCaregiver(id);
    }

    @GetMapping(path = "{id}")
    public ApiResponse getCaregiverById(@PathVariable("id") Long id) {
        return caregiverService.getCaregiverById(id);
    }

}
