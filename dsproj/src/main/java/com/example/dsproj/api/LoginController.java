package com.example.dsproj.api;


import com.example.dsproj.dto.LoginDTO;
import com.example.dsproj.model.ApiResponse;
import com.example.dsproj.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("api/v1")
@RestController
public class LoginController {

    private final LoginService loginService;


    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public ApiResponse login(@RequestBody LoginDTO loginDTO) {
        return loginService.login(loginDTO);
    }

}
