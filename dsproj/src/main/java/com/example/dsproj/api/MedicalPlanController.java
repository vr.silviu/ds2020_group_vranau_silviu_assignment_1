package com.example.dsproj.api;


import com.example.dsproj.dto.CaregiverDTO;
import com.example.dsproj.dto.MedicalPlanDTO;
import com.example.dsproj.model.ApiResponse;
import com.example.dsproj.service.MedicalPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("api/v1/medicalPlan")
@RestController
public class MedicalPlanController {
    private MedicalPlanService medicalPlanService;

    @Autowired

    public MedicalPlanController(MedicalPlanService medicalPlanService) {
        this.medicalPlanService = medicalPlanService;
    }

    @RequestMapping("/add/{idPatient}")
    @PostMapping
    public ApiResponse addMedicalPlan(@PathVariable(name = "idPatient") Long idPatient,
                                      @Valid @NonNull @RequestBody MedicalPlanDTO medicalPlanDTO) {

        return medicalPlanService.addMedicalPlan(idPatient,medicalPlanDTO);
    }

//    @GetMapping
//    public List<MedicalPlanDTO> getAllMedicationPlans() {
//        return medicalPlanService.getAllMedicationPlans();
 //   }
}
