//package com.example.dsproj.api;
//
//import com.example.dsproj.dto.RegisterDTO;
//import com.example.dsproj.model.ApiResponse;
//import com.example.dsproj.service.CarService;
//import com.example.dsproj.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RequestMapping("api/v1")
//@RestController
//public class UserController {
//    private final UserService userService;
//    @Autowired
//    public UserController(UserService userService) {
//        this.userService = userService;
//    }
//
//    @PostMapping("/register")
//    public ApiResponse signUp(@RequestBody RegisterDTO registerDTO){
//        return userService.register(registerDTO);
//    }
//}
