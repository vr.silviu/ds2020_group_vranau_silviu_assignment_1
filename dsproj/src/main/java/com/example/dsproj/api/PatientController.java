package com.example.dsproj.api;


import com.example.dsproj.dao.PatientDAO;
import com.example.dsproj.dto.PatientDTO;
import com.example.dsproj.model.ApiResponse;
import com.example.dsproj.model.Patient;
import com.example.dsproj.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("api/v1/patients")
@RestController
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping
    public List<PatientDTO> getAllPatients() {
        return patientService.getAllPatients();
    }

    @PostMapping
    public ApiResponse addOrUpdatePatient(@Valid @NonNull @RequestBody PatientDTO patientDTO) {
        return patientService.addOrUpdatePatient(patientDTO);
    }

    @DeleteMapping(path = "{id}")
    public ApiResponse deletePatientById(@PathVariable("id") Long id) {
        return patientService.deletePatient(id);
    }

    @GetMapping(path = "{id}")
    public ApiResponse getPatientById(@PathVariable("id") Long id) {
        return patientService.getPatientById(id);
    }
}
