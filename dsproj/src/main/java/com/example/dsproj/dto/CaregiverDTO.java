package com.example.dsproj.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashSet;
import java.util.Set;

public class CaregiverDTO {

    private Long id;
    private String name;
    private String email;
    private String birthDate;
    private String gender;
    private String address;
    private String password;
    private Boolean isCaregiver = true;

    private Set<PatientDTO> patients = new HashSet<>();

    public CaregiverDTO(Long id, String name, String email, String birthDate, String gender, String address, String password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.password = password;
    }

    public CaregiverDTO(String name, String email, String birthDate, String gender, String address, String password) {
        this.name = name;
        this.email = email;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.password = password;
    }

    public CaregiverDTO() {
    }

    public Set<PatientDTO> getPatients() {
        return patients;
    }

    public void setPatients(Set<PatientDTO> patients) {
        this.patients = patients;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getCaregiver() {
        return isCaregiver;
    }

    public void setCaregiver(Boolean caregiver) {
        isCaregiver = caregiver;
    }
}
