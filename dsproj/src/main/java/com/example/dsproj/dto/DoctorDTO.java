package com.example.dsproj.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class DoctorDTO {

    private Long id;
    private String name;
    private String email;
    private String password;
    private Boolean isDoctor = true;

    public DoctorDTO() {
    }

    public DoctorDTO(String name, String email, String password, Boolean isDoctor) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.isDoctor = isDoctor;
    }

    public DoctorDTO(Long id, String name, String email, String password, Boolean isDoctor) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.isDoctor = isDoctor;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String username) {
        this.email = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDoctor() {
        return isDoctor;
    }

    public void setDoctor(Boolean doctor) {
        isDoctor = doctor;
    }
}
