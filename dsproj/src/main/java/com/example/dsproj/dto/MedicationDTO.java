package com.example.dsproj.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.HashSet;
import java.util.Set;

public class MedicationDTO {

    private Long id;
    private String name;
    private String sideEffects;
    private String dosage;
    @JsonIgnore
    private Set<MedicalPlanDTO> medicalPlans = new HashSet<>();

    public MedicationDTO() {
    }

    public MedicationDTO(String name, String sideEffects, String dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public MedicationDTO(Long id, String name, String sideEffects, String dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Set<MedicalPlanDTO> getMedicalPlans() {
        return medicalPlans;
    }

    public void setMedicalPlans(Set<MedicalPlanDTO> medicalPlans) {
        this.medicalPlans = medicalPlans;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
