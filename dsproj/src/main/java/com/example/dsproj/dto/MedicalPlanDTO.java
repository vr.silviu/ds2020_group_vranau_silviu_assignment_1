package com.example.dsproj.dto;

import com.example.dsproj.model.Medication;
import com.example.dsproj.model.Patient;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MedicalPlanDTO {

    private Long id;
    private String doctorName;
    private String treatmentPeriod;
    @JsonIgnore
    private PatientDTO patient;
    private List<Long> medicationIds;
    private Set<MedicationDTO> medications = new HashSet<>();


    public MedicalPlanDTO() {
    }

    public MedicalPlanDTO(Long id, String doctorName, String treatmentPeriod) {
        this.id = id;
        this.doctorName = doctorName;
        this.treatmentPeriod = treatmentPeriod;
    }

    public MedicalPlanDTO(String doctorName, String treatmentPeriod) {
        this.doctorName = doctorName;
        this.treatmentPeriod = treatmentPeriod;
    }

    public PatientDTO getPatient() {
        return patient;
    }

    public void setPatientDTO(PatientDTO patientDTO) {
        this.patient = patientDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(String treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public Set<MedicationDTO> getMedications() {
        return medications;
    }

    public void setMedications(Set<MedicationDTO> medications) {
        this.medications = medications;
    }

    public List<Long> getMedicationIds() {
        return medicationIds;
    }

    public void setMedicationIds(List<Long> medicationIds) {
        this.medicationIds = medicationIds;
    }

    @Override
    public String toString() {
        return "MedicalPlanDTO{" +
                "id=" + id +
                ", doctorName='" + doctorName + '\'' +
                ", treatmentPeriod='" + treatmentPeriod + '\'' +
                ", patient=" + patient +
                ", medications=" + medications +
                '}';
    }
}
