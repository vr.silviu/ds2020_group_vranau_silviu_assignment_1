package com.example.dsproj.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashSet;
import java.util.Set;

public class PatientDTO {

    private Long id;
    private String name;
    private String birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private String email;
    private String password;
    private Boolean isPatient = true;
    private CaregiverDTO caregiverDTO;
    private Set<MedicalPlanDTO> medicalPlans = new HashSet<>();

    public PatientDTO() {
    }

    public PatientDTO(String name, String password, String birthDate, String email, String gender, String address, String medicalRecord) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.email = email;
        this.password = password;
    }

    public PatientDTO(Long id, String name, String password, String email, String birthDate, String gender, String address, String medicalRecord) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public Set<MedicalPlanDTO> getMedicalPlans() {
        return medicalPlans;
    }

    public void setMedicalPlans(Set<MedicalPlanDTO> medicalPlans) {
        this.medicalPlans = medicalPlans;
    }

    public CaregiverDTO getCaregiver() {
        return caregiverDTO;
    }

    public void setCaregiverDTO(CaregiverDTO caregiverDTO) {
        this.caregiverDTO = caregiverDTO;
    }

    public Boolean getPatient() {
        return isPatient;
    }

    public void setPatient(Boolean patient) {
        isPatient = patient;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
