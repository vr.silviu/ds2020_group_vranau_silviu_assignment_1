package com.example.dsproj.service;


import com.example.dsproj.dao.CaregiverDAO;
import com.example.dsproj.dao.DoctorDAO;
import com.example.dsproj.dao.PatientDAO;
import com.example.dsproj.dto.CaregiverDTO;
import com.example.dsproj.dto.DoctorDTO;
import com.example.dsproj.dto.LoginDTO;
import com.example.dsproj.dto.PatientDTO;
import com.example.dsproj.model.ApiResponse;
import com.example.dsproj.model.Caregiver;
import com.example.dsproj.model.Doctor;
import com.example.dsproj.model.Patient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {


    private final DoctorDAO doctorDAO;
    private final PatientDAO patientDAO;
    private final CaregiverDAO caregiverDAO;
    @Autowired
    public LoginService(DoctorDAO doctorDAO, PatientDAO patientDAO,CaregiverDAO caregiverDAO ) {
        this.doctorDAO = doctorDAO;
        this.patientDAO = patientDAO;
        this.caregiverDAO = caregiverDAO;
    }

    public ApiResponse login(LoginDTO loginDTO) {

        Patient patient = patientDAO.findByEmail(loginDTO.getEmail());
        if(patient == null) {
            Doctor doctor = doctorDAO.findByEmail(loginDTO.getEmail());
            if(doctor == null) {
                Caregiver caregiver = caregiverDAO.findByEmail(loginDTO.getEmail());
                    if(caregiver == null) {
                        return new ApiResponse(404,"No user found!", null);
                    }
                CaregiverDTO caregiverDTO = new CaregiverDTO(caregiver.getId(),
                        caregiver.getName(), caregiver.getEmail(),
                        caregiver.getBirthDate(), caregiver.getGender(), caregiver.getAddress(),
                        "");
                return new ApiResponse(404,"Login successful! Caregiver", caregiverDTO);
            } else if(doctor.getPassword().equals(loginDTO.getPassword()) == true) {
                DoctorDTO doctorDTO = new DoctorDTO(doctor.getId(),doctor.getName(), doctor.getEmail(), "", true);
                return new ApiResponse(200,"Login successful! Doctor", doctorDTO);
            } else {
                return new ApiResponse(404,"Username or password are incorrect!", null);
            }
        } else {
            PatientDTO patientDTO = new PatientDTO(
                    patient.getId(), patient.getName(), "",
                    patient.getEmail(), patient.getBirthDate(), patient.getGender(),
                    patient.getAddress(), patient.getMedicalRecord()
            );

            return new ApiResponse(200,"Login successful! Patient", patientDTO);
        }
    }
}
