package com.example.dsproj.service;

import com.example.dsproj.dao.PatientDAO;
import com.example.dsproj.dto.CaregiverDTO;
import com.example.dsproj.dto.MedicalPlanDTO;
import com.example.dsproj.dto.MedicationDTO;
import com.example.dsproj.dto.PatientDTO;
import com.example.dsproj.helpers.Helper;
import com.example.dsproj.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PatientService {


    private final PatientDAO patientDAO;
    @Autowired
    public PatientService(PatientDAO patientDAO) {
        this.patientDAO = patientDAO;
    }


    public List<PatientDTO> getAllPatients() {
        List<Patient> allPatients = patientDAO.findAll();
        List<PatientDTO> allPatientDTOs = new ArrayList<>();

        for(Patient patient: allPatients) {
            allPatientDTOs.add(createPatientDTO(patient));
        }
        return allPatientDTOs;
    }

    public ApiResponse addOrUpdatePatient(PatientDTO patientDTO) {
        if(patientDTO.getId() != null) {
            Optional<Patient> patient = patientDAO.findById(patientDTO.getId());
            if(patient.isPresent()) {
                Patient existingPatient = patient.get();
                Patient patient1 = new Patient(patientDTO.getName(), patientDTO.getEmail(),
                        patientDTO.getPassword(), patientDTO.getBirthDate(), patientDTO.getGender(),
                        patientDTO.getAddress(), patientDTO.getMedicalRecord(), patientDTO.getPatient());
                patient1.setMedicalPlans(existingPatient.getMedicalPlans());
                Helper.copyNonNullProperties(patient1, existingPatient); // this provides partial update.

                patientDAO.save(existingPatient);
                return new ApiResponse(200, "Patient updated", existingPatient);
            } else {
                return new ApiResponse(404, "Error! Patient does not exist!", null);
            }
        } else {
            Patient patient = new Patient(patientDTO.getName(),
                    patientDTO.getEmail(), patientDTO.getPassword(),
                    patientDTO.getBirthDate(), patientDTO.getGender(),
                    patientDTO.getAddress(), patientDTO.getMedicalRecord(),
                    patientDTO.getPatient());
            patientDAO.save(patient);
            return new ApiResponse(200, "Patient Recorded", patient);
        }
    }


    public ApiResponse deletePatient(Long id) {
        if(id != null) {
            Optional <Patient> patient = patientDAO.findById(id);
            if(patient.isPresent()) {
                patientDAO.deleteById(id);
                PatientDTO patientDTO = new PatientDTO();
                patientDTO.setId(patient.get().getId());
                patientDTO.setName(patient.get().getName());
                return new ApiResponse(200, "Patient deleted!", patientDTO);
            } else {
                return new ApiResponse(404, "Error! Patient does not exist!", null);
            }
        } else {
            return new ApiResponse(404, "End point does not contain id!", null);
        }
    }

    public ApiResponse getPatientById(Long id) {
        if(id != null) {
            Optional <Patient> patient = patientDAO.findById(id);
            if(patient.isPresent()) {
                Patient patientTemp = patient.get();
                PatientDTO patientDTO = createPatientDTO(patientTemp);;
                return new ApiResponse(200, "Patient found!", patientDTO);
            } else {
                return new ApiResponse(404, "Error! Patient does not exist!", null);
            }
        } else {
            return new ApiResponse(404, "End point does not contain id!", null);
        }
    }

    private PatientDTO createPatientDTO(Patient patientTemp) {
        PatientDTO patientDTO = new PatientDTO(patientTemp.getId(), patientTemp.getName(), "", patientTemp.getEmail(),
                patientTemp.getBirthDate(), patientTemp.getGender(), patientTemp.getAddress(), patientTemp.getMedicalRecord());

        Caregiver caregiver = patientTemp.getCaregiver();
        Set<MedicalPlan> medicalPlans = patientTemp.getMedicalPlans();
       // System.out.println(patientTemp.toString());
        if(caregiver != null) {
            CaregiverDTO caregiverDTO = new CaregiverDTO(caregiver.getId(), caregiver.getName(), caregiver.getEmail(), caregiver.getBirthDate(), caregiver.getGender(), caregiver.getAddress(), "");
            patientDTO.setCaregiverDTO(caregiverDTO);
        }
            Set<MedicalPlanDTO>medicalPlanDTOS = new HashSet<>();
            for(MedicalPlan medicalPlan: medicalPlans){
                MedicalPlanDTO medicalPlanDTO = new MedicalPlanDTO(medicalPlan.getId(), medicalPlan.getDoctorName(),
                        medicalPlan.getTreatmentPeriod());
                Set<Medication> medications = medicalPlan.getMedications();
                Set<MedicationDTO> medicationDTOS = new HashSet<>();
                for(Medication medication: medications) {
                    MedicationDTO medicationDTO = new MedicationDTO(medication.getId(),
                            medication.getName(), medication.getSideEffects(), medication.getDosage());
                    medicationDTOS.add(medicationDTO);
                }

                medicalPlanDTO.setMedications(medicationDTOS);
                medicalPlanDTOS.add(medicalPlanDTO);
            }
            patientDTO.setMedicalPlans(medicalPlanDTOS);

        return patientDTO;
    }


}
