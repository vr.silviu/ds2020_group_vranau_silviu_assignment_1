//package com.example.dsproj.service;
//
//import com.example.dsproj.dao.UserDAO;
//import com.example.dsproj.dto.RegisterDTO;
//import com.example.dsproj.model.ApiResponse;
//import com.example.dsproj.model.UserModel;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//
//@Service
//public class UserService {
//
//    UserDAO userDAO;
//    @Autowired
//    public UserService(UserDAO userDAO) {
//        this.userDAO = userDAO;
//    }
//
//    public ApiResponse register(RegisterDTO registerDTO) {
//        UserModel user = new UserModel();
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(4);
//        String encodedPassword = encoder.encode(registerDTO.getPassword());
//        BeanUtils.copyProperties(registerDTO, user);
//        user.setPassword(encodedPassword);
//        userDAO.save(user);
//        System.out.println(user.toString());
//        return new ApiResponse(200,"Register completed", user);
//    }
//}
