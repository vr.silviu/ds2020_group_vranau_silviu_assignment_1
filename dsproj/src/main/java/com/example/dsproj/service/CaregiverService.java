package com.example.dsproj.service;


import com.example.dsproj.dao.CaregiverDAO;
import com.example.dsproj.dao.PatientDAO;
import com.example.dsproj.dto.CaregiverDTO;
import com.example.dsproj.dto.PatientDTO;
import com.example.dsproj.helpers.Helper;
import com.example.dsproj.model.ApiResponse;
import com.example.dsproj.model.Caregiver;
import com.example.dsproj.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CaregiverService {

    private final CaregiverDAO caregiverDAO;
    private final PatientDAO patientDAO;

    @Autowired
    public CaregiverService(CaregiverDAO caregiverDAO, PatientDAO patientDAO) {
        this.caregiverDAO = caregiverDAO;
        this.patientDAO = patientDAO;
    }


    public List<CaregiverDTO> getAllCaregivers() {
        List<Caregiver> allCaregivers = caregiverDAO.findAll();
        List<CaregiverDTO> allCaregiverDTOS = new ArrayList<>();


        for(Caregiver caregiver: allCaregivers) {
            Set<PatientDTO> allPatientDTOS = new HashSet<>();

            CaregiverDTO caregiverDTO = new CaregiverDTO(
                    caregiver.getId(), caregiver.getName(), caregiver.getEmail(),
                    caregiver.getBirthDate(), caregiver.getGender(), caregiver.getAddress(), ""
            );
            System.out.println(caregiver.getName());
            for (Patient patient : caregiver.getPatients()) {
                System.out.println(patient.toString());
                PatientDTO patientDTO = new PatientDTO(patient.getId(), patient.getName(),
                        "", patient.getEmail(), patient.getBirthDate(), patient.getGender(),
                        patient.getAddress(), patient.getMedicalRecord());
                allPatientDTOS.add(patientDTO);
            }
            caregiverDTO.setPatients(allPatientDTOS);
            allCaregiverDTOS.add(caregiverDTO);
        }
        return allCaregiverDTOS;
    }

    public ApiResponse addOrUpdateCaregiver(CaregiverDTO caregiverDTO) {
        if(caregiverDTO.getId() != null) {
            Optional<Caregiver> caregiver = caregiverDAO.findById(caregiverDTO.getId());
            if(caregiver.isPresent()) {
                Caregiver existingCaregiver = caregiver.get();
                Set<Patient> pat = caregiver.get().getPatients();
                Helper.copyNonNullProperties(caregiverDTO, existingCaregiver); // this provides partial update.
                existingCaregiver.setPatients(pat);
                caregiverDAO.save(existingCaregiver);
                return new ApiResponse(200, "Caregiver updated", existingCaregiver);
            } else {
                return new ApiResponse(404, "Error! Caregiver does not exist!", null);
            }
        } else {
            Caregiver caregiver = new Caregiver(
                    caregiverDTO.getName(), caregiverDTO.getEmail(), caregiverDTO.getBirthDate(),
                    caregiverDTO.getGender(), caregiverDTO.getAddress(), caregiverDTO.getPassword()
            );
            caregiverDAO.save(caregiver);
            return new ApiResponse(200, "Caregiver Recorded", caregiver);
        }
    }

    public ApiResponse addPatientsToCaregiver(Long idCaregiver, Long idPatient) {
        if(idCaregiver != null) {
            Optional <Caregiver> caregiver = caregiverDAO.findById(idCaregiver);
            if(caregiver.isPresent()) {
                Caregiver tmp = caregiver.get();
                Set<Patient> patients = tmp.getPatients();

                Optional<Patient> pat = patientDAO.findById(idPatient);

                if(pat.isPresent()) {
                    patients.add(pat.get());
                }

                tmp.setPatients(patients);
                caregiverDAO.save(tmp);

                return new ApiResponse(200, "Assosiated Patient to Caregiver", tmp);
            } else {
                return new ApiResponse(404, "Error! Caregiver does not exist!", null);
            }
        } else {
            return new ApiResponse(404, "End point does not contain id!", null);
        }
    }

    public ApiResponse removePatientsFromCaregiver(Long caregiverId, Long idPatient) {
        if(caregiverId != null) {
            Optional <Caregiver> caregiver = caregiverDAO.findById(caregiverId);
            if(caregiver.isPresent()) {
                Caregiver tmp = caregiver.get();
                Set<Patient> patients = tmp.getPatients();


                Optional<Patient> pat = patientDAO.findById(idPatient);
                if(pat.isPresent() && patients.contains(pat.get())) {
                    patients.remove(pat.get());
                } else {
                    return new ApiResponse(404, "Error! Patient does not exist!", null);
                }

                tmp.setPatients(patients);
                caregiverDAO.save(tmp);

                return new ApiResponse(200, "Removed Patient/s from Caregiver", tmp);
            } else {
                return new ApiResponse(404, "Error! Caregiver does not exist!", null);
            }
        } else {
            return new ApiResponse(404, "End point does not contain id!", null);
        }
    }

    public ApiResponse deleteCaregiver(Long id) {
        if(id != null) {
            Optional <Caregiver> caregiver = caregiverDAO.findById(id);
            if(caregiver.isPresent()) {
                caregiverDAO.deleteById(id);
                CaregiverDTO caregiverDTO = new CaregiverDTO();
                caregiverDTO.setId(caregiver.get().getId());
                caregiverDTO.setName(caregiver.get().getName());
                return new ApiResponse(200, "Caregover deleted!", caregiverDTO);
            } else {
                return new ApiResponse(404, "Error! Caregiver does not exist!", null);
            }
        } else {
            return new ApiResponse(404, "End point does not contain id!", null);
        }
    }

    public ApiResponse getCaregiverById(Long id) {
        if(id != null) {
            Optional <Caregiver> caregiver = caregiverDAO.findById(id);
            if(caregiver.isPresent()) {
                Set<PatientDTO> allPatientDTOS = new HashSet<>();
                Caregiver caregiverTemp = caregiver.get();
                CaregiverDTO caregiverDTO = new CaregiverDTO(caregiverTemp.getId(),
                        caregiverTemp.getName(), caregiverTemp.getEmail(), caregiverTemp.getBirthDate(),
                        caregiverTemp.getBirthDate(), caregiverTemp.getAddress(), "");

                for (Patient patient : caregiverTemp.getPatients()) {
                    PatientDTO patientDTO = new PatientDTO(patient.getId(), patient.getName(),
                            "", patient.getEmail(), patient.getBirthDate(), patient.getGender(),
                            patient.getAddress(), patient.getMedicalRecord());
                    allPatientDTOS.add(patientDTO);
                }
                caregiverDTO.setPatients(allPatientDTOS);

                return new ApiResponse(200, "Caregiver found!", caregiverDTO);
            } else {
                return new ApiResponse(404, "Error! Caregiver does not exist!", null);
            }
        } else {
            return new ApiResponse(404, "End point does not contain id!", null);
        }
    }
}
