package com.example.dsproj.service;


import com.example.dsproj.dao.MedicationDAO;
import com.example.dsproj.dto.MedicalPlanDTO;
import com.example.dsproj.dto.MedicationDTO;
import com.example.dsproj.dto.PatientDTO;
import com.example.dsproj.helpers.Helper;
import com.example.dsproj.model.ApiResponse;

import com.example.dsproj.model.MedicalPlan;
import com.example.dsproj.model.Medication;
import com.example.dsproj.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MedicationService {

    private final MedicationDAO medicationDAO;

    @Autowired

    public MedicationService(MedicationDAO medicationDAO) {
        this.medicationDAO = medicationDAO;
    }

    public List<MedicationDTO> getAllMedication() {
        List<Medication> allMedications = medicationDAO.findAll();
        List<MedicationDTO> allMedicationDTOs = new ArrayList<>();

        for(Medication medication: allMedications) {
            MedicationDTO medicationDTO = new MedicationDTO(medication.getId(), medication.getName(), medication.getSideEffects(), medication.getDosage());
            allMedicationDTOs.add(medicationDTO);
        }
        return allMedicationDTOs;
    }

    public ApiResponse addOrUpdateMedication(MedicationDTO medicationDTO) {
        if(medicationDTO.getId() != null) {
            Optional<Medication> medication = medicationDAO.findById(medicationDTO.getId());
            if(medication.isPresent()) {
                Medication existingMedication = medication.get();
                Helper.copyNonNullProperties(medicationDTO, existingMedication); // this provides partial update.
                medicationDAO.save(existingMedication);
                return new ApiResponse(200, "Medicatinon updated", existingMedication);
            } else {
                return new ApiResponse(404, "Error! Medication does not exist!", null);
            }
        } else {
            Medication medication = new Medication(medicationDTO.getName(), medicationDTO.getSideEffects(), medicationDTO.getDosage());
            medicationDAO.save(medication);
            return new ApiResponse(200, "Medication Recorded", medication);
        }
    }

    public ApiResponse deleteMedication(Long id) {
        if(id != null) {
            Optional <Medication> medication = medicationDAO.findById(id);
            if(medication.isPresent()) {
                medicationDAO.deleteById(id);
                MedicationDTO medicationDTO = new MedicationDTO();
                medicationDTO.setId(medication.get().getId());
                medicationDTO.setName(medication.get().getName());
                return new ApiResponse(200, "Medication deleted!", medicationDTO);
            } else {
                return new ApiResponse(404, "Error! Medication does not exist!", null);
            }
        } else {
            return new ApiResponse(404, "End point does not contain id!", null);
        }
    }

    public ApiResponse getMedicationById(Long id) {
        if(id != null) {
            Optional <Medication> medication = medicationDAO.findById(id);
            if(medication.isPresent()) {
                Medication medicationTemp = medication.get();
                MedicationDTO medicationDTO = new MedicationDTO(
                        medicationTemp.getId(), medicationTemp.getName(),
                        medicationTemp.getSideEffects(),
                        medicationTemp.getDosage()
                );
                return new ApiResponse(200, "Medication found!", medicationDTO);
            } else {
                return new ApiResponse(404, "Error! Medication does not exist!", null);
            }
        } else {
            return new ApiResponse(404, "End point does not contain id!", null);
        }
    }
}
