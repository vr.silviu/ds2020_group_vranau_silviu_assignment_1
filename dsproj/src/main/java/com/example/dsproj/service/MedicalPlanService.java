package com.example.dsproj.service;


import com.example.dsproj.dao.MedicalPlanDAO;
import com.example.dsproj.dao.MedicationAndMedicalPlanDAO;
import com.example.dsproj.dao.MedicationDAO;
import com.example.dsproj.dao.PatientDAO;
import com.example.dsproj.dto.MedicalPlanDTO;
import com.example.dsproj.dto.MedicationDTO;
import com.example.dsproj.dto.PatientDTO;
import com.example.dsproj.model.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class MedicalPlanService {
    private MedicalPlanDAO medicalPlanDAO;
    private PatientDAO patientDAO;
    private MedicationDAO medicationDAO;
    private MedicationAndMedicalPlanDAO medicationAndMedicalPlanDAO;

    @Autowired
    public MedicalPlanService(MedicalPlanDAO medicalPlanDAO,
                              PatientDAO patientDAO, MedicationDAO medicationDAO,
                              MedicationAndMedicalPlanDAO medicationAndMedicalPlanDAO) {
        this.patientDAO = patientDAO;
        this.medicalPlanDAO = medicalPlanDAO;
        this.medicationDAO = medicationDAO;
        this.medicationAndMedicalPlanDAO = medicationAndMedicalPlanDAO;
    }


    public ApiResponse addMedicalPlan(Long id, MedicalPlanDTO medicalPlanDTO) {

        System.out.println(medicalPlanDTO.toString());
        MedicalPlan medicalPlan = new MedicalPlan(medicalPlanDTO.getDoctorName(),
                medicalPlanDTO.getTreatmentPeriod());
        Optional<Patient> patient = patientDAO.findById(id);
        if(patient.isPresent()){
            medicalPlan.setPatient(patient.get());
        }
        System.out.println(medicalPlan.toString());
        medicalPlanDAO.save(medicalPlan);
        System.out.println(medicalPlanDTO.getMedicationIds());
        Set<Medication> medicationToAdd = new HashSet<>();
        for(Long idMed: medicalPlanDTO.getMedicationIds()) {
            Optional<Medication> medication = medicationDAO.findById(idMed);
            if(medication.isPresent()){
                medicationToAdd.add(medication.get());
                System.out.println(medication.get().getName());
            }
        }

        for(Medication medication: medicationToAdd) {
            MedicationAndMedicalPlan medicationAndMedicalPlan = new MedicationAndMedicalPlan(
                    medication.getId(), medicalPlan.getId()
            );
            medicationAndMedicalPlanDAO.save(medicationAndMedicalPlan);
        }


        return new ApiResponse(200, "MedicalPlan Recorded", medicalPlan);
    }


}
