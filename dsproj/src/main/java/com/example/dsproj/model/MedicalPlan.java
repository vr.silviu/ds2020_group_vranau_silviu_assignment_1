package com.example.dsproj.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "medicalPlan")
public class MedicalPlan {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String doctorName;
    private String treatmentPeriod;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }, mappedBy = "medicalPlans")
    private Set<Medication> medications = new HashSet<>();

    public MedicalPlan() {
    }

    public MedicalPlan(Long id, String doctorName, String treatmentPeriod) {
        this.id = id;
        this.doctorName = doctorName;
        this.treatmentPeriod = treatmentPeriod;
    }

    public MedicalPlan(String doctorName, String treatmentPeriod) {
        this.doctorName = doctorName;
        this.treatmentPeriod = treatmentPeriod;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(String treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

}
