package com.example.dsproj.model;

public enum Role {
//    USER("User"),
//    STATE_ADMIN("StateAdmin"),
    DOCTOR("Doctor"),
    CAREGIVER("Caregiver"),
    PATIENT("Patient");
   // SELLER("Seller");

    private String role;

    Role(String role) {
        this.role = role;
    }
    public String getRole() {
        return role;
    }
}
