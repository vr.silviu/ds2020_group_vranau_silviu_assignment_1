package com.example.dsproj.model;

import java.io.Serializable;
import java.util.Objects;

public class MedicationMedicalPlanId implements Serializable {

    private Long idMedication;
    private Long idMedicalPlan;

    public MedicationMedicalPlanId() {
    }

    public MedicationMedicalPlanId(Long idMedication, Long idMedicalPlan) {
        this.idMedication = idMedication;
        this.idMedicalPlan = idMedicalPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationMedicalPlanId that = (MedicationMedicalPlanId) o;
        return Objects.equals(idMedication, that.idMedicalPlan) &&
                Objects.equals(idMedicalPlan, that.idMedication);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMedication, idMedicalPlan);
    }
}
