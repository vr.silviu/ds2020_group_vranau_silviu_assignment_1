package com.example.dsproj.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    private String birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private String password;
    private Boolean isPatient = true;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    //@JoinColumn(foreignKey =  @ForeignKey(name = "caregiver_id"), name = "caregiver_id")
    private Caregiver caregiver;

    @JoinColumn(name = "patient_id")
    @OneToMany(targetEntity = MedicalPlan.class)
    private Set<MedicalPlan> medicalPlans = new HashSet<>();

    public Patient() {
    }

    public Patient(String name, String email, String password,
                   String birthDate, String gender,
                   String address, String medicalRecord,
                   Boolean isPatient) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.email = email;
        this.password = password;
        this.isPatient = isPatient;
    }
    public Patient(Long id, String email, String name, String password,
                   String birthDate, String gender,
                   String address, String medicalRecord,
                   Boolean isPatient) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.id = id;
        this.email = email;
        this.password = password;
        this.isPatient = isPatient;
    }



    public Set<MedicalPlan> getMedicalPlans() {
        return medicalPlans;
    }

    public void setMedicalPlans(Set<MedicalPlan> medicalPlans) {
        this.medicalPlans = medicalPlans;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public Boolean getPatient() {
        return isPatient;
    }

    public void setPatient(Boolean patient) {
        isPatient = patient;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

}
