package com.example.dsproj.model;

import javax.persistence.*;


@Entity
@Table(name = "medicationandmedicalplan")
@IdClass(MedicationMedicalPlanId.class)
public class MedicationAndMedicalPlan {

    @Id
    @Column(name = "idmedication")
    private Long idMedication;
    @Id
    @Column(name = "idmedicalplan")
    private Long idMedicalPlan;

    public MedicationAndMedicalPlan() {
    }


    public MedicationAndMedicalPlan(Long idMedication, Long idMedicalPlan) {
        this.idMedication = idMedication;
        this.idMedicalPlan = idMedicalPlan;

    }

    public Long getIdMedication() {
        return idMedication;
    }

    public void setIdMedication(Long idMedication) {
        this.idMedication = idMedication;
    }

    public Long getIdMedicalPlan() {
        return idMedicalPlan;
    }

    public void setIdMedicalPlan(Long idMedicalPlan) {
        this.idMedicalPlan = idMedicalPlan;
    }

}
