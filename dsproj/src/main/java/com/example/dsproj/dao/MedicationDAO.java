package com.example.dsproj.dao;

import com.example.dsproj.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationDAO extends JpaRepository<Medication, Long> {
}
