package com.example.dsproj.dao;

import com.example.dsproj.model.MedicationAndMedicalPlan;
import com.example.dsproj.model.MedicationMedicalPlanId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationAndMedicalPlanDAO extends JpaRepository<MedicationAndMedicalPlan, MedicationMedicalPlanId> {
}
