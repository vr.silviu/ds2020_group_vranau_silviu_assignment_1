package com.example.dsproj.dao;

import com.example.dsproj.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface DoctorDAO extends JpaRepository<Doctor, Long> {
    Doctor findByEmail(String email);
}
