package com.example.dsproj.dao;

import com.example.dsproj.model.Caregiver;
import com.example.dsproj.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaregiverDAO extends JpaRepository<Caregiver, Long> {
    Caregiver findByEmail(String email);
}
