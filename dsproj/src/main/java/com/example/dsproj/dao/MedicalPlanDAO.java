package com.example.dsproj.dao;

import com.example.dsproj.model.MedicalPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicalPlanDAO extends JpaRepository<MedicalPlan, Long> {
}
